using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
    public static PlayerManager Instance;

    public float Damage => damage;
    [SerializeField] private float damage = 50;

    public float CurrentHealth { get; private set; }
    [SerializeField] private float health = 100;

    public int Score { get; set; }

    private void Awake() {
        if (!Instance) Instance = this;
    }

    // Start is called before the first frame update
    void Start() {
        Init();
    }

    public void Init() {
        CurrentHealth = health;
        UIManager.Instance.SetupSliderData(0, CurrentHealth);
    }

	private void OnTriggerEnter(Collider other) {
        if (other.gameObject.TryGetComponent(out Enemy enemy)) {
            enemy.DamagePlayer();
			PoolingManager.Instance.DeactivateEnemy(enemy);
        }
    }

    public void TakeDamage(float damage) {
        CurrentHealth -= damage;
        if (CurrentHealth <= 0) {
            UIManager.Instance.ShowLosingPanel();
            Time.timeScale = 0;
            Debug.Log($"Player died");
		}
	}
}
