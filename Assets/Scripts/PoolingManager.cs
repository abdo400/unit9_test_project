using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PoolingManager : MonoBehaviour {
	public static PoolingManager Instance;

	[SerializeField] private GameObject enemyPrefab;
	
	[NonSerialized] public List<Enemy> activeEnemies = new List<Enemy>();
	public Queue<Enemy> enemyPool = new Queue<Enemy>();

	[SerializeField] private float radius = 10;

	private void Awake() {
		if (!Instance) Instance = this;
	}

	public void DeactivateEnemy(Enemy enemy) {
		if (activeEnemies.Contains(enemy)) {
			activeEnemies.Remove(enemy);
		}
		enemy.Deactivate();
		enemyPool.Enqueue(enemy);
	}

	public void ActivateEnemy() {
		if (enemyPool.Count > 0) {
			var enemy = enemyPool.Dequeue();
			activeEnemies.Add(enemy);
			enemy.Activate();
		} else {
			var enemyGO = Instantiate(enemyPrefab, GetRandomPointOnCircleEdge(), Quaternion.identity, transform);
			activeEnemies.Add(enemyGO.GetComponent<Enemy>());
		}
	}
	public Vector3 GetRandomPointOnCircleEdge() {
		var vector2 = UnityEngine.Random.insideUnitCircle.normalized * radius;
		return new Vector3(vector2.x, 1.4f, vector2.y);
	}

}
