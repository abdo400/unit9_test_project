using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
	public static UIManager Instance;

	[SerializeField] private TextMeshProUGUI scoreText;
	[SerializeField] private Slider healthSlider;
	[SerializeField] private CanvasGroup losingPanelCG;
	[SerializeField] private TextMeshProUGUI panelScoreText;

	private void Awake() {
		if (!Instance) Instance = this;
	}

	private void Start() {
		losingPanelCG.alpha = 0;
	}

	// Update is called once per frame
	void Update() {
		var score = PlayerManager.Instance ? PlayerManager.Instance.Score : 0;
		scoreText.text = $"SCORE: {score}";
		var health = PlayerManager.Instance ? PlayerManager.Instance.CurrentHealth : 0;
		healthSlider.value = health;
	}

	public void SetupSliderData(float minValue, float maxValue) {
		healthSlider.minValue = minValue;
		healthSlider.maxValue = maxValue;
		healthSlider.value = maxValue;
	}

	public void ShowLosingPanel() {
		losingPanelCG.alpha = 1;
		panelScoreText.text = $"YOUR CURRENT SCORE IS: {PlayerManager.Instance.Score}";
	}

	public void Restart() {
		PlayerManager.Instance.Init();
		EnemyManager.Instance.Init();
		losingPanelCG.alpha = 0;
		Time.timeScale = 1;
	}
}
