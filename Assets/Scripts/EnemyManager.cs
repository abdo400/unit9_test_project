using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {
	public static EnemyManager Instance;


	[SerializeField] private float waveDelay = 1f;
	private float waveDelayCounter;
	[SerializeField] private int waveMaxEnemies = 10;
	private int waveEnemyCount = 0;
	private bool waveStarted;
	private int waveCounter = 0;
	
	[Space]
	[SerializeField] private float enemyDelay = 1;
	private float enemyDelayCounter;

	[Space]
	[SerializeField] private Transform playerTransform;

	private void Awake() {
		if (!Instance) Instance = this;
	}

	// Start is called before the first frame update
	void Start() {
		Init();
	}

	public void Init() {
		waveStarted = false;
		waveDelayCounter = Time.time;
		enemyDelayCounter = Time.time;
	}

	// Update is called once per frame
	void Update() {
		StartWave();
		InstantiateEnemies();
		MoveEnemies();
	}

	private void StartWave() {
		if (!waveStarted && Time.time - waveDelayCounter >= waveDelay) {
			waveStarted = true;
			waveCounter++;
		}
	}

	private void InstantiateEnemies() {
		if (waveStarted) {
			if (Time.time - enemyDelayCounter >= enemyDelay) {
				PoolingManager.Instance.ActivateEnemy();
				enemyDelayCounter = Time.time;
				waveEnemyCount++;
				if (waveEnemyCount >= waveMaxEnemies) {
					waveStarted = false;
					waveEnemyCount = 0;
					waveDelayCounter = Time.time;
				}
			}
		}
	}

	private void MoveEnemies() {
		var poolingManager = PoolingManager.Instance;
		for (int i = 0; i < poolingManager.activeEnemies.Count; i++) {
			poolingManager.activeEnemies[i].Move(playerTransform.position);
		}
	}
}
