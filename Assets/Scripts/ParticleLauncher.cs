using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLauncher : MonoBehaviour {

	private ParticleSystem particleLauncher;
	[SerializeField] private ParticleSystem splatterParticles;


	private List<ParticleCollisionEvent> collisionEvents;

	private void Start() {
		Init();
	}

	private void Init() {
		particleLauncher = GetComponent<ParticleSystem>();
		collisionEvents = new List<ParticleCollisionEvent>();
	}

	// Update is called once per frame
	void Update() {
		if (Input.GetMouseButton(0)) {
			particleLauncher.Emit(1);
		}
	}

	private void OnParticleCollision(GameObject other) {
		ParticlePhysicsExtensions.GetCollisionEvents(particleLauncher, other, collisionEvents);
		for (int i = 0; i < collisionEvents.Count; i++) {
			var collisionEvent = collisionEvents[i]; 
			EmitAtLocation(collisionEvent);
			var colliderGO = collisionEvent.colliderComponent.gameObject;
			if (colliderGO.layer == 7) {
				if (colliderGO.TryGetComponent(out Enemy enemy)) {
					enemy.TakeDamage(PlayerManager.Instance.Damage);
					PlayerManager.Instance.Score += enemy.Score;
				}
			}
		}
	}

	private void EmitAtLocation(ParticleCollisionEvent particleCollisionEvent) {
		splatterParticles.transform.position = particleCollisionEvent.intersection;
		splatterParticles.transform.rotation = Quaternion.LookRotation(particleCollisionEvent.normal);
		splatterParticles.Emit(1);
	}
}
