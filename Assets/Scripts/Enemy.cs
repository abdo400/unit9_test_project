using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	[SerializeField] private float speed;
	public float Damage => damage;
	[SerializeField] private float damage = 10;

	[SerializeField] private float health = 100;
	public int Score => score;
	[SerializeField] private int score = 5;

	public float Health { get; private set; }

	public void DamagePlayer() {
		PlayerManager.Instance.TakeDamage(damage);
	}

	public void TakeDamage(float damage) {
		Health -= damage;
		if (Health <= 0) {
			PoolingManager.Instance.DeactivateEnemy(this);
		}
	}

	public void Activate() {
		gameObject.SetActive(true);
		transform.position = PoolingManager.Instance.GetRandomPointOnCircleEdge();
		Health = health;
	}

	public void Deactivate() {
		gameObject.SetActive(false);
	}

	public void Move(Vector3 targetPosition) {
		transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
	}

}
