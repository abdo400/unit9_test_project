using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {
	public float speedH = 2f;
	private float yaw = 0;

	// Update is called once per frame
	void Update() {
		yaw += speedH * Input.GetAxis("Mouse X");
		transform.eulerAngles = new Vector3(0, yaw, 0);
	}
}
