-- Hello --
- The working scene is "MainScene". Found at Assets/Scenes
- To run the app locally in the Unity Editor, please enable the "Camera Manager" component. Found in Player => Main Camera. This is to be able to rotate the camera by mouse in the Unity Editor.
- To run the build on mobile. Disable the "Camera Manager". Build and run and it should run fine.

Some parameters to play with:
[EnemyManager]
This is responsible for enemy behavior.
WaveDelay: how much seconds between each wave.
WaveMaxEnemies: how much enemies for each wave.
EnemyDelay: how much time between each enemy spawn (while awve spawning is active).

[PoolingManager]
This is a simple pooling system for instantiating enemies to save up performance.
Radius: how far away from the player enemies can be spawned.

[UIManager]
Responsible for UI management.

[Player]
Resposible for player behavior.
Damage: how much damage the player can do by shooting enemies.
Health: max player's health

[ParticleLauncher]
Responsible for shooting particles and making a nice splash effect. We use particle system for bullets since this is better for performance, and handle its pooling by itself.

[Enemy]
Holds enemy data.
Speed: the speed the enemy moves at.
Damage: the damage the enemy can affect the player with on collision.
Health: enemy health.
Score: the score it gives the player on enemy death by shooting

